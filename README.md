# README #

## WHAT IS IT? ##

A unicode-friendly, chatroom web app (in React) which leverages a REST API and Websockets (in Go) for sending/receiving live messages.
The messaging functionality exposed by websockets is a subset of that provided by the RESTful HTTP API, which could be used
by e.g. external API's that wish to send messages without having to use WebSockets.

Currently, any messages created through the REST API and not through WebSockets are correctly propagated to WebSocket clients as well.

## BUILD ##
The following steps should do the trick

1. cd $GOPATH/src
2. git clone https://bitbucket.org/jmr0/chat-api.git
3. cd ./chat-api
4. sqlite3 app.db < ./app.sql #Builds DB schema
5. go get
6. go build
7. cd ./client
8. npm install
9. npm run build
10. ./chat-api #Actually starts app
11. ./sample_data.sh #Populates app with sample rooms, users, and user-room permissions

## USAGE NOTES ##

The React app can be reached at http://localhost:8080, specific REST endpoints are listed under chat-api/api/rest.go

The UI is extremely primitive and requires inputting user ID and room ID to join a chatroom.
`sample_data.sh` creates users with ID's 1-10 and puts users 1-5 in room 1 and 6-10 in room 2.
It will 404 if attempting to log into a room that doesn't exist or with a user that doesn't exist or into a room where
a user is not "active" (i.e. not permitted). __Always re-run step 4 if re-running step 11__.

## TRADE-OFFS ##
Learned React in the process of writing this as I figured it'd feel light-weight and result in less code - happy
with the results but forgive any blunders/anti-patterns. I also bootstrapped it with the create-react-app to avoid managing
all the dependencies, so there may be some cruft lingering from that.

sqlite was used for simplicity in deployment, but a more robust RDBMS like PG should be used instead.

I chose not to use any external SQL library for go, so there's a bit of boilerplate in `data` but I figured the number of queries
did not warrant external dependencies.

The `data` package (for DB calls) currently also handles product-logic and is coupled with the HTTP/WebSocket service calls,
but it avoids unnecessary code at the moment and is separate enough that it can be refactored. Its interface is also easy to replicate
by a service that caches those DB queries, for example.

## MISSING / KNOWN BUGS ##

A list of things that need to happen:

+ Automated testing
+ The DELETE and PUT verbs of the API
+ Active rooms currently leak (problematic with a large number of rooms) need teardown call when all clients exit
+ Ways to manage rooms and users from the UI
+ Better UI in general (listing connected and active users for each room, better error display/handling)
+ Reload UI chatroom with user metadata if a new user joins said room
+ Establishing a message retrieval limit from the API
+ Establishing a message length limit
+ Organize logging better (currently very verbose)
+ Richer messaging scheme for WebSockets to communicate chatroom events
