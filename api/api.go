package api

import (
	"chat-api/util"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"net/http"
)

// Structs used by the API to unmarshal HTTP/WebSocket requests
type User struct {
	Name string `schema:"name"`
}

type UserKey struct {
	Id int `schema:"id"`
}

type Room struct {
	Name string `schema:"name"`
}

type RoomKey struct {
	Id int `schema:"id"`
}

type UserInRoom struct {
	UserId int `schema:"user_id"`
	RoomId int `schema:"room_id"`
}

type SendMessage struct {
	Body   string `schema:"body" json:"body"`
	UserId int    `schema:"user_id" json:"user_id"`
	RoomId int    `schema:"room_id" json:"room_id"`
}

//Implemented by HTTP/WebSocket services

type ApiService interface {
	Start(chan error)
}

//Utility function for marshaling requests

func marshal(d *schema.Decoder, res interface{}, r *http.Request, fields ...string) error {

	params, err := util.LoadParams(r, mux.Vars(r), fields...)
	if err != nil {
		return fmt.Errorf("Error extracting parameters: %s", err.Error())
	}

	err = d.Decode(res, params)
	if err != nil {
		return fmt.Errorf("Error decoding data structure: %s", err.Error())
	}

	return nil
}
