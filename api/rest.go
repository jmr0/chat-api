package api

import (
	"chat-api/data"
	"chat-api/util"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"log"
	"net/http"
)

//HTTP API

type HttpServer struct {
	ds         *data.Store
	decoder    *schema.Decoder
	msgEmitter func(*data.Message) //Callback issued when a new message is created
	port       int
}

func NewHttpServer(ds *data.Store, port int, msgEmitter func(*data.Message)) *HttpServer {

	//Decoder unmarshals HTTP/WebSocket requests, relying on the schema struct tag
	decoder := schema.NewDecoder()
	return &HttpServer{ds: ds,
		decoder:    decoder,
		msgEmitter: msgEmitter,
		port:       port}
}

func (s *HttpServer) Start(errChan chan error) {

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", s.ServeApp)
	router.HandleFunc("/users", s.CreateUser).Methods("POST")
	router.HandleFunc("/users/{id}", s.GetUser).Methods("GET")
	router.HandleFunc("/users/{id}/rooms", s.UserRooms).Methods("GET")
	router.HandleFunc("/rooms", s.CreateRoom).Methods("POST")
	router.HandleFunc("/rooms/{id}", s.GetRoom).Methods("GET")
	router.HandleFunc("/rooms/{id}/users", s.RoomUsers).Methods("GET")
	router.HandleFunc("/rooms/{id}/messages", s.ReadMessages).Methods("GET")
	router.HandleFunc("/rooms/{room_id}/users/{user_id}", s.JoinRoom).Methods("POST")
	router.HandleFunc("/rooms/{room_id}/messages", s.CreateMessage).Methods("POST")

	//Serve static files
	dir := "./client/build"
	router.PathPrefix("/static/").Handler(http.FileServer(http.Dir(dir)))

	go func() {
		errChan <- http.ListenAndServe(fmt.Sprintf(":%d", s.port), router)
	}()
}

//Serve React App
func (s *HttpServer) ServeApp(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./client/build/index.html")
}

func (s *HttpServer) CreateUser(w http.ResponseWriter, r *http.Request) {
	var err error
	var userId int

	defer func() { util.Respond(w, r, &userId, err) }()

	var userReq User
	err = s.marshal(&userReq, r, "name")
	if err != nil {
		return
	}

	userId, err = s.ds.NewUser(userReq.Name)
}

func (s *HttpServer) UserRooms(w http.ResponseWriter, r *http.Request) {
	var err error
	var rooms []*data.Room

	defer func() { util.Respond(w, r, &rooms, err) }()

	var userReq UserKey
	err = s.marshal(&userReq, r, "id")
	if err != nil {
		return
	}

	rooms, err = s.ds.ActiveRooms(userReq.Id)

}

func (s *HttpServer) JoinRoom(w http.ResponseWriter, r *http.Request) {
	var err error
	var sessionId int

	defer func() { util.Respond(w, r, &sessionId, err) }()

	var userRoomReq UserInRoom
	err = s.marshal(&userRoomReq, r, "user_id", "room_id")
	if err != nil {
		return
	}

	sessionId, err = s.ds.JoinRoom(userRoomReq.RoomId, userRoomReq.UserId)

}

func (s *HttpServer) RoomUsers(w http.ResponseWriter, r *http.Request) {
	var err error
	var users []*data.User

	defer func() { util.Respond(w, r, &users, err) }()

	var roomReq RoomKey
	err = s.marshal(&roomReq, r, "id")
	if err != nil {
		return
	}

	users, err = s.ds.RoomUsers(roomReq.Id)

}

func (s *HttpServer) CreateRoom(w http.ResponseWriter, r *http.Request) {
	var err error
	var roomId int

	defer func() { util.Respond(w, r, &roomId, err) }()

	var roomReq Room
	err = s.marshal(&roomReq, r, "name")
	if err != nil {
		return
	}

	roomId, err = s.ds.NewRoom(roomReq.Name)
}

func (s *HttpServer) ReadMessages(w http.ResponseWriter, r *http.Request) {
	var err error
	var messages []*data.Message

	defer func() { util.Respond(w, r, &messages, err) }()

	var roomReq RoomKey
	err = s.marshal(&roomReq, r, "id")
	if err != nil {
		return
	}

	messages, err = s.ds.GetMessages(roomReq.Id)
}

func (s *HttpServer) GetUser(w http.ResponseWriter, r *http.Request) {
	var err error
	var user *data.User

	defer func() { util.Respond(w, r, user, err) }()

	var userReq data.User
	err = s.marshal(&userReq, r, "id")
	if err != nil {
		return
	}

	user, err = s.ds.GetUser(userReq.Id)
}

func (s *HttpServer) GetRoom(w http.ResponseWriter, r *http.Request) {
	var err error
	var room *data.Room

	defer func() { util.Respond(w, r, room, err) }()

	var roomReq data.Room
	err = s.marshal(&roomReq, r, "id")
	if err != nil {
		return
	}

	room, err = s.ds.GetRoom(roomReq.Id)
}

func (s *HttpServer) CreateMessage(w http.ResponseWriter, r *http.Request) {
	var err error
	var message *data.Message

	defer func() { util.Respond(w, r, message, err) }()

	var sendMsg SendMessage
	err = s.marshal(&sendMsg, r, "body", "room_id", "user_id")
	if err != nil {
		return
	}

	message, err = s.ds.NewMessage(sendMsg.UserId, sendMsg.RoomId, sendMsg.Body)

	if err == nil {
		log.Printf("Emitting message %v\n", message)
		s.msgEmitter(message)
	}
}

func (s *HttpServer) marshal(res interface{}, r *http.Request, fields ...string) error {
	return marshal(s.decoder, res, r, fields...)
}
