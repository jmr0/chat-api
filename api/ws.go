package api

import (
	"chat-api/data"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

//WebSocket API

type WebSocketChatRoom struct {
	ds       *data.Store
	decoder  *schema.Decoder
	manager  *roomManager
	messages chan SendMessage
	route    string
	port     int
}

func NewWebSocketChatRoom(ds *data.Store, userRoomRoute string, port int) *WebSocketChatRoom {
	//Decoder unmarshals HTTP/WebSocket requests, relying on the schema struct tag
	decoder := schema.NewDecoder()

	manager := newRoomManager(ds)
	go manager.start()
	return &WebSocketChatRoom{ds: ds,
		decoder: decoder,
		manager: manager,
		route:   userRoomRoute,
		port:    port}
}

func (cr *WebSocketChatRoom) Start(errChan chan error) {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/ws/{user_id}/{room_id}", cr.handleRequest)
	go func() {
		errChan <- http.ListenAndServe(fmt.Sprintf(":%d", cr.port), router)
	}()
}

//Exposes the Chatroom to other services that need to propagate messages (the HTTP service in this case)
func (cr *WebSocketChatRoom) Receive(msg *data.Message) {

	resp := make(chan *liveRoom)
	cr.manager.routeRequest <- roomRequest{roomId: msg.RoomId, resp: resp}
	lr := <-resp

	lr.forward <- msg

}

func (cr *WebSocketChatRoom) handleRequest(w http.ResponseWriter, r *http.Request) {

	var userRoomReq UserInRoom
	err := cr.marshal(&userRoomReq, r, "user_id", "room_id")
	if err != nil {
		http.NotFound(w, r)
		return
	}

	//Verify that user is allowed in room
	users, err := cr.ds.RoomUsers(userRoomReq.RoomId)
	exists := false
	for _, u := range users {
		if u.Id == userRoomReq.UserId {
			exists = true
			break
		}
	}

	if !exists {
		http.NotFound(w, r)
		return
	}

	//Switch to WebSocket from HTTP
	conn, err := (&websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}).Upgrade(w, r, nil)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	//Connect client to room
	resp := make(chan *liveRoom)
	cr.manager.routeRequest <- roomRequest{roomId: userRoomReq.RoomId, resp: resp}
	lr := <-resp
	client := &roomClient{
		userId:  userRoomReq.UserId,
		roomId:  userRoomReq.RoomId,
		sock:    conn,
		send:    make(chan []byte),
		manager: cr.manager}

	lr.connect <- client

	//Start websocket listening/sending
	go client.read()
	go client.write()
}

//Represent an active room and connected clients,
//receives requests through its various channels
type liveRoom struct {
	id         int
	clients    map[*roomClient]bool
	message    chan *SendMessage  //Handles new messages received from a client through websocket
	forward    chan *data.Message //Fans out messages that were received from the REST API to clients
	connect    chan *roomClient   //Connects a new client
	disconnect chan *roomClient   //Disonnected an existing client
	ds         *data.Store
}

func newLiveRoom(roomId int, ds *data.Store) *liveRoom {
	r := &liveRoom{id: roomId}
	r.clients = make(map[*roomClient]bool)
	r.message = make(chan *SendMessage)
	r.forward = make(chan *data.Message)
	r.connect = make(chan *roomClient)
	r.disconnect = make(chan *roomClient)
	r.ds = ds
	return r
}

func (r *liveRoom) start() {
	log.Printf("Starting live room for %d\n", r.id)
	for {
		select {
		case c := <-r.connect:
			log.Printf("Client %d connecting to %d\n", c.userId, r.id)
			r.clients[c] = true
		case c := <-r.disconnect:
			log.Printf("Client %d disconnecting from %d\n", c.userId, r.id)
			if present := r.clients[c]; present {
				r.remove(c)
			}
		case fwdMsg := <-r.forward:
			log.Printf("Forwarding message %+v\n", *fwdMsg)
			r.send(fwdMsg)
		case sendMsg := <-r.message:
			log.Printf("Received message %+v\n", *sendMsg)
			msg, err := r.ds.NewMessage(sendMsg.UserId, sendMsg.RoomId, sendMsg.Body)
			if err != nil {
				log.Printf("Error creating message %v: %s\n", msg, err.Error())
				continue
			}
			r.send(msg)
		}
	}
}

func (r *liveRoom) remove(c *roomClient) {
	close(c.send)
	delete(r.clients, c)
}

/*
Fans out a message to all the clients connected to the room (open websockets),
removing any failed clients
*/
func (r *liveRoom) send(msg *data.Message) {
	content, _ := json.Marshal(msg)
	for c := range r.clients {
		log.Printf("Sending message %+v to %d\n", *msg, c.userId)
		select {
		case c.send <- content:
		default:
			log.Printf("Message %+v failed to send %d\n", *msg, c.userId)
			r.remove(c)
		}
	}
}

type roomClient struct {
	userId  int
	roomId  int
	send    chan []byte
	sock    *websocket.Conn
	manager *roomManager
}

//Read from client WebSocket
func (rc *roomClient) read() {
	resp := make(chan *liveRoom)
	rc.manager.routeRequest <- roomRequest{roomId: rc.roomId, resp: resp}
	lr := <-resp

	defer func() {
		log.Printf("Disconnecting socket for user %d in room %d\n", rc.userId, rc.roomId)
		lr.disconnect <- rc
		rc.sock.Close()
	}()

	for {
		_, msgRaw, err := rc.sock.ReadMessage()
		if err != nil {
			log.Printf("Disconnecting socket for user %d in %d: %s\n", rc.userId, rc.roomId, err.Error())
			lr.disconnect <- rc
			rc.sock.Close()
			break
		}
		log.Printf("User %d in room %d got message %+v \n", rc.roomId, rc.userId, msgRaw)
		var msg SendMessage
		err = json.Unmarshal(msgRaw, &msg)
		if err != nil {
			log.Printf("Error processing message %s: %s\n", msgRaw, err.Error())
		}
		lr.message <- &msg
	}
}

//Writes to client's websocket
func (rc *roomClient) write() {
	defer func() {
		rc.sock.Close()
	}()

	for {
		select {
		case msg, ok := <-rc.send:
			if !ok {
				rc.sock.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			log.Printf("Sending %d a message in room %d\n", rc.userId, rc.roomId)
			rc.sock.WriteMessage(websocket.TextMessage, msg)
		}

	}
}

/*Request rooms from roomManager, responses are sent back through
the `resp` channel
*/
type roomRequest struct {
	roomId int
	resp   chan *liveRoom
}

/*
Manages life-cycle of active rooms. Other routines can query it by
issuing requests to routeRequest and passing a response channel
*/
type roomManager struct {
	rooms        map[int]*liveRoom
	routeRequest chan roomRequest
	ds           *data.Store
}

func newRoomManager(ds *data.Store) *roomManager {
	r := &roomManager{}
	r.rooms = make(map[int]*liveRoom)
	r.routeRequest = make(chan roomRequest)
	r.ds = ds
	return r
}

func (rm *roomManager) start() {
	rm.rooms = make(map[int]*liveRoom)

	for {
		select {
		case req := <-rm.routeRequest:
			lr := rm.rooms[req.roomId]
			if lr == nil {
				log.Printf("No existing room %d, creating\n", req.roomId)
				lr = newLiveRoom(req.roomId, rm.ds)
				rm.rooms[req.roomId] = lr
				go lr.start()
			}
			req.resp <- lr
		}
	}
}

func (cr *WebSocketChatRoom) marshal(res interface{}, r *http.Request, fields ...string) error {
	return marshal(cr.decoder, res, r, fields...)
}
