CREATE TABLE users(
   id INTEGER PRIMARY KEY AUTOINCREMENT     NOT NULL,
   name           TEXT    NOT NULL
);

CREATE UNIQUE INDEX users_name
on users (name);

CREATE TABLE messages(
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	user_id INTEGER NOT NULL,
	room_id INTEGER NOT NULL,
	body TEXT NOT NULL
);

CREATE INDEX room_messages
on messages (room_id);

CREATE TABLE rooms(
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	name TEXT NOT NULL
);

CREATE TABLE active_rooms(
	user_id INTEGER NOT NULL,
	room_id INTEGER NOT NULL,
	PRIMARY KEY (user_id, room_id)
);

CREATE INDEX rooms_active_rooms
on active_rooms (room_id);
