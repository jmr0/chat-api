import React, { Component } from 'react';
import './App.css';
import Chat from './Chat';
import conf from './conf';

class App extends Component {

  constructor(props) {
	  super(props);
	  this.state = { user: '', room: '', error: '' };

	  this.userChangeHandler = this.userChangeHandler.bind(this);
	  this.userSubmitHandler = this.userSubmitHandler.bind(this);
	  this.roomSubmitHandler = this.roomSubmitHandler.bind(this);
	  this.roomChangeHandler = this.roomChangeHandler.bind(this);
  }

  render() {

	if (this.state.error) {
		return (<h2>{this.state.error}</h2>)
	}

	if (this.state.roomName) {
		return (
			<Chat user={this.state.user}
			  room={this.state.room}
			  userName={this.state.userName}
			  roomName={this.state.roomName}
			  roomUsers={this.state.roomUsers}
			/>
		);
	}

	var changeHandler = this.userChangeHandler;
	var subHandler = this.userSubmitHandler;
	var placeHolder = "Enter user ID";
	if (this.state.userName) {
		changeHandler = this.roomChangeHandler;
		subHandler = this.roomSubmitHandler;
		placeHolder = "Enter room ID";
	}

    return (
      <form className="user-form" onSubmit={subHandler}>
		<h1>Chat It Up</h1>
		<div>
			<input type="text" ref={(input) => {this.textInput = input; }} placeholder={placeHolder} onChange={changeHandler} />
		</div>
		<input type="submit" value="Submit" />
      </form>
    );
  }

  userChangeHandler(event) {
	this.setState({
		user: event.target.value
	});
  }

  userSubmitHandler(event) {
	event.preventDefault(); //Prevent POST

	const currentUser = this.state.user;
	fetch(`/users/${currentUser}`).then(resp => {
		if (!resp.ok) {
			throw Error(resp.statusText);
		}
		return resp.json();
	}).then(j =>{
		this.setState({
			userName: j['name'],
			user: currentUser
		});
		this.textInput.value = '';
	}).catch(e =>{
		this.setState({
			error: e.message
		});
	});

  }

  roomChangeHandler(event) {
	this.setState({
		userName: this.state.userName,
		user: this.state.user,
		room: event.target.value
	});
  }

  roomSubmitHandler(event) {
	event.preventDefault(); //Prevent POST

	const fetchJson = url => {
		return fetch(url).then( resp => {
			if (!resp.ok) {
				throw Error(resp.statusText);
			}
			return resp.json();
		})
	};

	//Declare endpoints
	const currentRoom = this.state.room;
	const roomDataUrl =  `/rooms/${currentRoom}`;
	const roomUsersUrl = `${roomDataUrl}/users`;

	Promise.all([roomDataUrl, roomUsersUrl].map(fetchJson)).then(jsons => {
			const roomName = jsons[0]['name'];
			const roomUsers = {};
			jsons[1].forEach(user => {
				roomUsers[user['id']] = user['name'];
			});
			this.setState({
				userName: this.state.userName,
				user: this.state.user,
				room: this.state.room,
				roomName: roomName,
				roomUsers: roomUsers
			});
	}).catch(e=>{
		this.setState({
			error: e.message
		});
	});

	this.textInput.value = '';
  }


}

export default App;
