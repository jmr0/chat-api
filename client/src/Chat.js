import React, { Component } from 'react';
import './App.css';
import Messages from './Messages';
import MessageInput from './MessageInput';
import conf from './conf';

class Chat extends Component {

	constructor(props) {
		super(props);

		this.state = {messages: [], connected: false, error: ''};
		this.messageSendHandler = this.messageSendHandler.bind(this);
		this.addMessage = this.addMessage.bind(this);

		const endpoint = `ws://${window.location.hostname}:${conf.wsPort}/ws/${this.props.user}/${this.props.room}`;
		this.sock = new WebSocket(endpoint);

		this.sock.onopen = (ev) => {
			this.setConnected(true);
		};

		this.sock.onclose = (ev) => {
			this.setConnected(false);
			this.state.error = "Disconnected from server";
		};

		this.sock.onmessage = (ev) => {
			this.addMessage(JSON.parse(ev.data));
		};
	}

	render() {

		if (this.state.error) {
			return (<h2>Error: {this.state.error}</h2>);
		}

		return (
			<div className="container">
				<h1>Chat</h1>
				<h3>User: {this.props.userName} | Room: {this.props.roomName} | WebSocket Status: {this.state.connected ? "Connected" : "Disconnected"} </h3>
				<Messages messages={this.state.messages} roomUsers={this.props.roomUsers} />
				<MessageInput sendCallback={this.messageSendHandler} />
			</div>
	    );
	}

	messageSendHandler(message) {
		const msg = {
			user_id: parseInt(this.props.user),
			room_id: parseInt(this.props.room),
			body: message
		};

		this.sock.send(JSON.stringify(msg));
	}

	setConnected(connected) {
		this.setState({messages: this.state.messages, connected: connected});
	}

	addMessage(msg) {
		const msges = this.state.messages;
		msges.push(msg);
		this.setState({messages: msges});
	}

	componentDidMount() {
		const endPoint = `/rooms/${this.props.room}/messages`;
		fetch(endPoint).then(resp => {
			if (!resp.ok) {
				throw Error(resp.statusText);
			}
			return resp.json();
		}).then(j =>{
			this.setState({
				messages: this.state.messages.concat(j)
			});
		}).catch(e =>{
			this.setState({
				messages: [],
				error: e.message
			});
		});
	}

}

export default Chat;
