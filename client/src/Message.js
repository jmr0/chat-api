import React, { Component } from 'react';
import './App.css';

class Message extends Component {

	render() {
		return (
			<div className="message">
				<p> {this.props.userName}: {this.props.body} </p>
			</div>
	    );
	}

}

export default Message;
