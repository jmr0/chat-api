import React, { Component } from 'react';
import './App.css';

class MessageInput extends Component {

  constructor(props) {
	  super(props);
	  this.state = { body: '' };

	  this.submitHandler = this.submitHandler.bind(this);
	  this.changeHandler = this.changeHandler.bind(this);
  }

	render() {
		return (
			<form id="msginput" onSubmit={this.submitHandler}>
				<input type="text" onChange={this.changeHandler} value={this.state.body}/>
			</form>
	    );
	}

	submitHandler(event) {
		event.preventDefault();
		this.props.sendCallback(this.state.body);
		this.setState({body: ''});
	}

	changeHandler(event) {
		this.setState({body: event.target.value});
	}

}

export default MessageInput;
