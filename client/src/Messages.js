import React, { Component } from 'react';
import './App.css';
import Message from './Message';

class Messages extends Component {

	render() {
		//TODO need to filter duplicates if there's contention between WebSocket and REST
		const messages = this.props.messages.map((message) => {
			return (
				<Message
					key={message.id}
					body={message.body}
					user={message.user_id}
					userName={this.props.roomUsers[message.user_id]}
					mine={message.mine}
				/>
			);
		});

		return (
			<ul id="message-list" ref={(mlist) => { this.messageList = mlist; }}>
				{ messages }
			</ul>
		);
	}

	componentDidUpdate() {
		this.messageList.scrollTop = this.messageList.scrollHeight;
	}

}

export default Messages;
