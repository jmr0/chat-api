//Used to properly encode form data for AJAX POSt requests
export default {
	encode: (data) => {
		const body = Object.keys(data).map(key=>encodeURIComponent(key)+'='+encodeURIComponent(data[key])).join('&');
		return body;
	};
}
