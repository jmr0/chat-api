package config

var (
	HttpPort      int    = 8080
	WebSocketPort int    = 9090
	DbPath        string = "app.db"
	Debug         bool   = false //Determines what kind of errors are sent to client
)
