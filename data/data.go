package data

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log"
)

type User struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type Message struct {
	Id     int    `json:"id"`
	Body   string `json:"body"`
	UserId int    `json:"user_id"`
	RoomId int    `json:"room_id"`
}

type Room struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type Store struct {
	db            *sql.DB
	newRoom       *sql.Stmt
	room          *sql.Stmt
	newUser       *sql.Stmt
	user          *sql.Stmt
	newMessage    *sql.Stmt
	messages      *sql.Stmt
	userJoinsRoom *sql.Stmt
	activeRooms   *sql.Stmt
	roomUsers     *sql.Stmt
}

func ConnectDb(dbPath string) (*Store, error) {
	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		return nil, err
	}

	ds, err := NewStore(db)
	if err != nil {
		return nil, err
	}

	return ds, nil
}

/*
This sets up all the queries used by the application as prepared statements,
which has the added benefit of exposing any SQL errors as soon as the application starts
*/
func NewStore(db *sql.DB) (*Store, error) {

	ds := &Store{
		db: db,
	}

	queries := []struct {
		query string
		stmt  **sql.Stmt
	}{
		{"INSERT INTO rooms (name) VALUES (?)", &ds.newRoom},
		{"SELECT id, name FROM rooms WHERE id = ?", &ds.room},
		{"INSERT INTO users (name) VALUES (?)", &ds.newUser},
		{"SELECT ID, name FROM users WHERE id = ?", &ds.user},
		{"INSERT INTO messages (room_id, user_id, body) VALUES (?, ? ,?)", &ds.newMessage},
		{"SELECT id, room_id, user_id, body FROM messages where room_id = ? ORDER BY room_id ASC", &ds.messages},
		{"INSERT INTO active_rooms (user_id, room_id) VALUES (?, ?)", &ds.userJoinsRoom},
		{"SELECT r.id, r.name FROM active_rooms a JOIN rooms r WHERE a.room_id = r.id AND a.user_id = ?", &ds.activeRooms},
		{"SELECT u.id, u.name FROM active_rooms a JOIN users u WHERE a.user_id = u.id AND a.room_id = ? ", &ds.roomUsers},
	}

	for _, s := range queries {
		var err error
		*s.stmt, err = ds.db.Prepare(s.query)
		if err != nil {
			return nil, fmt.Errorf("Failed to prepare query %s: %s", s.query, err.Error())
		}
	}

	return ds, nil
}

func (ds *Store) NewRoom(name string) (int, error) {
	if name == "" {
		return 0, fmt.Errorf("Room name cannot be blank")
	}
	log.Printf("Creating room with name %s\n", name)
	res, err := ds.newRoom.Exec(name)
	return getId(res, err)
}

func (ds *Store) GetRoom(roomId int) (*Room, error) {

	var r Room
	err := ds.room.QueryRow(roomId).Scan(&r.Id, &r.Name)
	if err != nil {
		return nil, err
	}

	return &r, nil
}

func (ds *Store) GetUser(userId int) (*User, error) {
	var u User
	err := ds.user.QueryRow(userId).Scan(&u.Id, &u.Name)
	if err != nil {
		return nil, err
	}

	return &u, nil
}

func (ds *Store) NewUser(name string) (int, error) {
	if name == "" {
		return 0, fmt.Errorf("User name cannot be blank")
	}
	log.Printf("Creating user with name %s\n", name)
	res, err := ds.newUser.Exec(name)
	return getId(res, err)
}

func (ds *Store) NewMessage(userId int, roomId int, body string) (*Message, error) {
	rooms, err := ds.ActiveRooms(userId)
	if err != nil {
		log.Println("Could not create message from user %d in room %d: %s \n", userId, roomId, err.Error())
		return nil, err
	}

	//Users that have not joined the room cannot send messages
	var isActive bool

	for _, r := range rooms {
		if r.Id == roomId {
			isActive = true
			break
		}
	}

	if !isActive {
		return nil, fmt.Errorf("User %d not active in Room %d\n", userId, roomId)
	}

	res, err := ds.newMessage.Exec(roomId, userId, body)
	id, err := getId(res, err)
	if err != nil {
		return nil, fmt.Errorf("Failed to create message from user %d in room %d\n", userId, roomId)
	}
	return &Message{Id: id, Body: body, UserId: userId, RoomId: roomId}, nil
}

func (ds *Store) GetMessages(roomId int) ([]*Message, error) {

	//Ensure room exists
	_, err := ds.GetRoom(roomId)
	if err != nil {
		return nil, err
	}

	rows, err := ds.messages.Query(roomId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var messages []*Message
	for rows.Next() {
		var m Message
		err := rows.Scan(&m.Id, &m.RoomId, &m.UserId, &m.Body)
		if err != nil {
			return nil, err
		}

		messages = append(messages, &m)
	}

	return messages, nil

}

func (ds *Store) JoinRoom(roomId int, userId int) (int, error) {

	//Ensure both room and user exist
	_, err := ds.GetRoom(roomId)
	if err != nil {
		return 0, err
	}

	_, err = ds.GetUser(userId)
	if err != nil {
		return 0, err
	}

	res, err := ds.userJoinsRoom.Exec(userId, roomId)
	return getId(res, err)
}

func (ds *Store) RoomUsers(roomId int) ([]*User, error) {
	//Ensure room exists
	_, err := ds.GetRoom(roomId)
	if err != nil {
		return nil, err
	}

	rows, err := ds.roomUsers.Query(roomId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var users []*User
	for rows.Next() {
		var u User
		err := rows.Scan(&u.Id, &u.Name)
		if err != nil {
			return nil, err
		}

		users = append(users, &u)
	}

	return users, nil
}

func (ds *Store) ActiveRooms(userId int) ([]*Room, error) {

	//Ensure user exists
	_, err := ds.GetUser(userId)
	if err != nil {
		return nil, err
	}

	rows, err := ds.activeRooms.Query(userId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var rooms []*Room
	for rows.Next() {
		var r Room
		err := rows.Scan(&r.Id, &r.Name)
		if err != nil {
			return nil, err
		}

		rooms = append(rooms, &r)
	}

	return rooms, nil
}

func getId(res sql.Result, err error) (int, error) {
	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil //TODO: should respect int64
}
