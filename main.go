package main

import (
	"chat-api/api"
	"chat-api/config"
	"chat-api/data"
	"log"
)

func main() {
	ds, err := data.ConnectDb(config.DbPath)
	if err != nil {
		log.Fatalf("Unable to establish connection to database: %s", err)
	}

	serverFailure := make(chan error)

	chatRoom := api.NewWebSocketChatRoom(ds, "/ws/{user_id}/{room_id}", config.WebSocketPort)
	chatRoom.Start(serverFailure)

	httpServer := api.NewHttpServer(ds, config.HttpPort, func(msg *data.Message) {
		chatRoom.Receive(msg)
	})
	httpServer.Start(serverFailure)

	log.Println("Servers up")
	log.Fatal(<-serverFailure)

}
