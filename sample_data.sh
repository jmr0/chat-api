#!/bin/sh

#Create users
curl -d "name=Rachel" -X POST http://localhost:8080/users
curl -d "name=Keith" -X POST http://localhost:8080/users
curl -d "name=Florene" -X POST http://localhost:8080/users
curl -d "name=John" -X POST http://localhost:8080/users
curl -d "name=Etha" -X POST http://localhost:8080/users
curl -d "name=Blake" -X POST http://localhost:8080/users
curl -d "name=Maria" -X POST http://localhost:8080/users
curl -d "name=Avi" -X POST http://localhost:8080/users
curl -d "name=Sam" -X POST http://localhost:8080/users
curl -d "name=Nathan" -X POST http://localhost:8080/users

#Create rooms
curl -d "name=FirstRoom" -X POST http://localhost:8080/rooms
curl -d "name=SecondRoom" -X POST http://localhost:8080/rooms

#Allow certain users in room 1
curl -X POST http://localhost:8080/rooms/1/users/1
curl -X POST http://localhost:8080/rooms/1/users/2
curl -X POST http://localhost:8080/rooms/1/users/3
curl -X POST http://localhost:8080/rooms/1/users/4
curl -X POST http://localhost:8080/rooms/1/users/5

#Allow certain users in room 2
curl -X POST http://localhost:8080/rooms/2/users/6
curl -X POST http://localhost:8080/rooms/2/users/7
curl -X POST http://localhost:8080/rooms/2/users/8
curl -X POST http://localhost:8080/rooms/2/users/9
curl -X POST http://localhost:8080/rooms/2/users/10

