package util

import (
	"chat-api/config"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"reflect"
)

//Used by services wishing to respond in JSON
func Respond(w http.ResponseWriter, r *http.Request, v interface{}, err error) {
	w.Header().Set("Content-Type", "application/json")
	enc := json.NewEncoder(w)
	if err != nil {
		http.Error(w, "", http.StatusBadRequest)
		var errMsg string

		//Choose whether to obscure error from client
		if config.Debug {
			errMsg = err.Error()
		} else {
			errMsg = "An error has occurred"
		}

		enc.Encode(map[string]string{"error": errMsg})
	} else {
		fixNils(v)
		err = enc.Encode(v)
		if err != nil {
			log.Fatal(fmt.Sprintf("JSON Encoding failed: %s", err.Error()), r, false)
			http.Error(w, "", http.StatusInternalServerError)
			enc.Encode(map[string]string{"error": err.Error()})
		}
	}
}

//Used to aid in marshaling requests
func LoadParams(r *http.Request, routeVars map[string]string, params ...string) (map[string][]string, error) {

	finalParams := make(map[string][]string)

	var err error
	if err = r.ParseForm(); err != nil {
		return nil, err
	}

	for _, p := range params {
		if res, ok := r.Form[p]; ok {
			finalParams[p] = res
		}

		if res, ok := routeVars[p]; ok {
			finalParams[p] = []string{res}
		}
	}

	return finalParams, nil
}

//Ensures that various Go types that can be "nil" are instead displayed
//as empty types (e.g. empty array and not "null"), this generally makes server
//responses a lot easier to handle
func fixNils(v interface{}) {
	rv := reflect.ValueOf(v)
	if rv.Kind() == reflect.Ptr {
		rv = rv.Elem()
	}

	if !rv.CanSet() {
		panic(fmt.Sprintf("Object of type %v is not settable", rv.Type()))
	}
	fixNilsReflect(rv)
}

func fixNilsReflect(v reflect.Value) {
	t := v.Type()

	switch t.Kind() {
	case reflect.Slice:
		if v.IsNil() {
			v.Set(reflect.MakeSlice(t, 0, 0))
		} else {
			for i := 0; i < v.Len(); i++ {
				fixNilsReflect(v.Index(i))
			}
		}
	case reflect.Map:
		if v.IsNil() {
			v.Set(reflect.MakeMap(t))
		}
	case reflect.Struct:
		for i := 0; i < v.NumField(); i++ {
			fixNilsReflect(v.Field(i))
		}
	case reflect.Ptr:
		if !v.IsNil() {
			fixNilsReflect(v.Elem())
		}
	case reflect.Interface:
		if !v.IsNil() {
			fixNilsReflect(v.Elem())
		}
	case reflect.Array:
		for i := 0; i < v.Len(); i++ {
			fixNilsReflect(v.Index(i))
		}
	}
}
